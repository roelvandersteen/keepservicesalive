﻿using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace KeepServicesAlive
{
    internal static class LoggerConfiguration
    {
        public static void Configure(string path)
        {
            var repository = (Hierarchy)LogManager.GetRepository();

            var layout = new PatternLayout {ConversionPattern = "%date %message%newline"};
            layout.ActivateOptions();

            var appender = new RollingFileAppender
            {
                AppendToFile = true,
                File = path,
                Layout = layout,
                MaxSizeRollBackups = 10,
                MaximumFileSize = "1MB",
                RollingStyle = RollingFileAppender.RollingMode.Size,
                StaticLogFileName = true
            };
            appender.ActivateOptions();

            repository.Root.AddAppender(appender);
            repository.Root.Level = Level.Info;

            repository.Configured = true;
        }
    }
}
