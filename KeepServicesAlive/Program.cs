﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using log4net;
using Microsoft.Win32.TaskScheduler;

namespace KeepServicesAlive
{
    internal static class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Program));

        [STAThread]
        public static void Main()
        {
            CreateScheduledTask();

            var configFile = Path.ChangeExtension(GetAssemblyFullPath(), "config");
            var logFile = Path.ChangeExtension(configFile, ".log");
            LoggerConfiguration.Configure(logFile);

            var serviceNames = GetServiceNames(configFile).Distinct().ToList();
            try
            {
                var services = ServiceController.GetServices().Where(x => serviceNames.Contains(x.ServiceName));
                foreach (ServiceController sc in services)
                {
                    serviceNames.Remove(sc.ServiceName);
                    if (sc.Status != ServiceControllerStatus.Stopped)
                    {
                        continue;
                    }

                    Logger.Info($"Start {sc.ServiceName}");
                    sc.Start();
                }

                if (serviceNames.Any())
                {
                    Logger.Warn($"Service{(serviceNames.Count == 1 ? "" : "s")} not found on this machine: {string.Join(", ", serviceNames)}");
                }
            }
            catch (Exception e)
            {
                Logger.Fatal(e.Message, e);
            }
        }

        private static void CreateScheduledTask()
        {
            using (var ts = new TaskService())
            {
                if (ts.AllTasks.Any(t => t.Name == "KeepServicesAlive"))
                {
                    return;
                }

                TaskDefinition td = ts.NewTask();
                td.RegistrationInfo.Description = "Check services and start them if necessary";
                td.Triggers.Add(new DailyTrigger
                {
                    StartBoundary = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Unspecified),
                    DaysInterval = 1,
                    Repetition = new RepetitionPattern(TimeSpan.FromMinutes(11.41667), TimeSpan.FromMinutes(1439))
                });
                td.Actions.Add(new ExecAction(Path.ChangeExtension(GetAssemblyFullPath(), "exe")));

                ts.RootFolder.RegisterTaskDefinition("KeepServicesAlive", td);
            }
        }

        private static string GetAssemblyFullPath()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            return Path.GetFullPath(path);
        }

        private static IEnumerable<string> GetServiceNames(string path)
        {
            return File.ReadLines(path).Where(s => !Regex.IsMatch(s, @"^\s*[/:#']")).Select(s => Regex.Replace(s.Trim(), @"\s.*", ""))
                .Where(s => !string.IsNullOrEmpty(s));
        }
    }
}