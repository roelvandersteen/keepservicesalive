KeepServicesAlive
=================

Check Windows services and start them if necessary.

- Run the executable from an administrative prompt to install the scheduled task.
- Create `KeepServicesAlive.config` in the install directory.

The config file lists all the service names to check. Every service must be on a separate line and must be installed on the local machine. E.g.:

```
St2x.MetarService
St2x.RowingUpdaterService
```